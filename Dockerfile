FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt
WORKDIR ./scannect/
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh