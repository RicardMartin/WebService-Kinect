import json
from binascii import a2b_base64
import uuid
import os

from django.contrib.auth.decorators import login_required
from django.core.files import File
from django.shortcuts import get_list_or_404, get_object_or_404
from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect
from django.http import HttpResponse

from ..business import get_user_models, save_preview_file, get_user_favourites, get_user_upvotes
from ..forms.scan_forms import UploadFileForm
from ..forms.user_forms import EditUserForm, EditProfileForm
from ..models.scan_models import ScanModel, user_image_path
from ..models.user_models import CustomUser
from scannect.settings import MEDIA_ROOT


@login_required
def upload(request):
    '''
    Method to upload scan models if the request method is POST.
    Otherwise return the view with the form to be able to upload it.
    :param request:
    :return:
    '''
    if request.method == 'POST':
        file_form = UploadFileForm(request.POST, request.FILES)
        if file_form.is_valid():
            file_model = file_form.save(commit=False)
            file_model.user = request.user
            file_model.save()
            if request.POST['preview_image']:
                pre_img = a2b_base64(request.POST['preview_image'].split('base64,')[1])
                path = user_image_path(file_model, '%s.png' % uuid.uuid4())
                image = open(os.path.join(MEDIA_ROOT, path), 'wb')
                image.write(pre_img)
                image.close()
                file_model.preview_image = path
                file_model.save()
            return redirect('home')
        else:
            return render(request, 'upload.html', {'file_form': file_form})
    else:
        file_form = UploadFileForm()
        return render(request, 'upload.html', {'file_form': file_form})


@login_required
def user_models(request):
    '''
    Get all scan models of the user who requested it.
    :param request:
    :return:
    '''
    models_list = get_user_models(request.user)

    page = request.GET.get('page', 1)

    paginator = Paginator(models_list, 12)
    try:
        models = paginator.page(page)
    except PageNotAnInteger:
        models = paginator.page(1)
    except EmptyPage:
        models = paginator.page(paginator.num_pages)

    return render(request, 'user_models.html', {'models': models})


@login_required
def edit(request, id):
    '''
    NOT USED. View for editing a scan model.
    :param request:
    :param id: Scan model ID
    :return:
    '''
    model = get_object_or_404(ScanModel, id=id)
    if request.method == 'POST':
        edit_form = UploadFileForm(request.POST, request.FILES, instance=model)
        if edit_form.is_valid():
            model = edit_form.save(commit=False)
            model.user = request.user
            model.save()
    else:
        edit_form = UploadFileForm(instance=model)
    return render(request, 'edit_model.html', {
        'edit_form': edit_form,
        'model_url': 'http://2.152.105.193:8000/media/' + str(model.file),
        'model_id': id
    })


@login_required
def delete_model(request, id):
    '''
    Delete a scan model.
    :param request:
    :param id: Scan model ID
    :return:
    '''
    if request.method == 'POST':
        try:
            ScanModel.objects.get(pk=id, user=request.user.id).delete()
        except ScanModel.DoesNotExist:
            messages.error(request, 'El modelo seleccionado no existe.')
    return redirect('myscans')

@login_required
def download_model(request, id):
    '''
    Download a scan model.
    :param request:
    :param id: Scan model ID
    :return:
    '''
    try:
        ScanModel.objects.get(pk=id).file
    except ScanModel.DoesNotExist:
        messages.error(request, 'El modelo seleccionado no existe.')
    return redirect('myscans')


@login_required
def profile(request):
    '''
    View of user profile.
    :param request:
    :return:
    '''
    return render(request, 'profile.html')


@login_required
def settings(request):
    '''
    Edit personal information about the request user.
    :param request:
    :return:
    '''
    password_form = PasswordChangeForm(user=request.user)
    if request.method == 'POST':
        form = EditUserForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            if request.FILES and 'image' in request.FILES:
                profile_form = EditProfileForm(request.POST, request.FILES, instance=request.user.profile)
                if profile_form.is_valid():
                    profile_form.save()
            return redirect('settings')
        else:
            return render(request, 'settings.html',
                          {
                              'edit_form': form,
                              'profile_form': profile_form,
                              'password_form': password_form,
                          })
    form = EditUserForm(instance=request.user)
    profile_form = EditProfileForm(instance=request.user.profile)
    return render(request, 'settings.html',
                  {
                      'edit_form': form,
                      'profile_form': profile_form,
                      'password_form': password_form,
                  })


@login_required
def upvote(request, id):
    '''
    API method. Upvote a scan model.
    :param request:
    :param id: Scan model id.
    :return: HttpResponse
    '''
    if request.method == 'POST':
        try:
            user = CustomUser.objects.get(pk=request.user.id)
            scan = ScanModel.objects.get(pk=id)
        except (ScanModel.DoesNotExist, CustomUser.DoesNotExist):
            return HttpResponse('Error', status=500)
        
        if scan.user == request.user:
            return HttpResponse('No autolike pls', status=500)

        if scan in get_user_upvotes(request.user):
            user.profile.upvotes.remove(scan)
            response_text = 'Upvoted successfully'
        else:
            user.profile.upvotes.add(scan)
            response_text = 'Upvote removed successfully'
        user.save()
        return HttpResponse(response_text, status=200)


@login_required
def favourite(request, id):
    '''
    API method. Add to favourites a scan model.
    :param request:
    :param id: Scan model id.
    :return: HttpResponse
    '''
    if request.method == 'POST':
        try:
            user = CustomUser.objects.get(pk=request.user.id)
            scan = ScanModel.objects.get(pk=id)
        except (ScanModel.DoesNotExist, CustomUser.DoesNotExist):
            return HttpResponse('Error', status=500)
        
        if scan.user == request.user:
            return HttpResponse('No autofav pls', status=500)

        if scan in get_user_favourites(request.user):
            user.profile.favourites.remove(scan)
            response_text = 'Added to favourite successfully'
        else:
            user.profile.favourites.add(scan)
            response_text = 'Removed from favourites successfully'
        user.save()
        return HttpResponse(response_text, status=200)


@login_required
def delete_account(request):
    '''
    Delete the request user account.
    :param request:
    :return:
    '''
    if request.method == 'POST':
        try:
            CustomUser.objects.get(pk=request.user.id).delete()
        except CustomUser.DoesNotExist:
            messages.error(request, 'No existe el usuario.')
    return redirect('home')


@login_required
def change_password(request):
    '''
    Change password of request user.
    :param request:
    :return:
    '''
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Password changed successfully')
        else:
            messages.error(request, "Couldn't change the password")
        return redirect('settings')


@login_required
def tmp_file(request):
    '''
    Save a temporary 3D file.
    :param request:
    :return: HttpResponse
    '''
    if request.method == 'POST' and request.FILES:
        stl_file = request.FILES['stl_file']
        if not stl_file.name.lower().endswith(('.stl', )):
            return HttpResponse(json.dumps({'message': 'File must be .stl'}), status=400)
        url = save_preview_file(stl_file)
        return HttpResponse(json.dumps({'url': url}), status=200)
    return HttpResponse(json.dumps({'message': 'The petition must be POST type and send a file'}), status=400)


@login_required
def favourites(request):
    '''
    View request user favourites scan models.
    :param request:
    :return:
    '''
    models_list = get_user_favourites(request.user)

    page = request.GET.get('page', 1)

    paginator = Paginator(models_list, 12)
    try:
        models = paginator.page(page)
    except PageNotAnInteger:
        models = paginator.page(1)
    except EmptyPage:
        models = paginator.page(paginator.num_pages)

    return render(request, 'user_favourites.html', {'models': models})
