from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, logout
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import Http404
from django.shortcuts import get_list_or_404, get_object_or_404

from ..models.user_models import CustomUser
from ..models.scan_models import ScanModel
from ..business import get_all_models, get_scan_model, get_user_models
from ..forms.user_forms import SignUpForm, LoginForm


def signup(request):
    '''
    Sign up a new user.
    :param request:
    :return:
    '''
    signup_form = SignUpForm(request.POST or None)
    if request.method == 'POST' and signup_form.is_valid():
        signup_form.save()
        messages.success(request, 'User registered succesfully!')
        return redirect('home')
    login_form = LoginForm()
    return render(request, 'signup.html', {'signup_form': signup_form, 'login_form': login_form})


def logout_user(request):
    '''
    Logout the current user.
    :param request:
    :return:
    '''
    logout(request)
    return redirect('home')


def home(request):
    '''
    Display scan models.
    :param request:
    :return:
    '''
    next = request.GET.get('next', None)
    filtro = request.GET.get('filter', 'date_desc')
    login_form = LoginForm(request.POST or None)
    if request.POST and login_form.is_valid():
        user = login_form.login(request)
        if user:
            login(request, user)
            if next:
                return redirect(next)
            else:
                return redirect('home')
    models_list = get_all_models(filtro)

    page = request.GET.get('page', 1)

    paginator = Paginator(models_list, 12)
    try:
        models = paginator.page(page)
    except PageNotAnInteger:
        models = paginator.page(1)
    except EmptyPage:
        models = paginator.page(paginator.num_pages)

    return render(request, 'home.html', {
        'login_form': login_form,
        'models': models,
        'filter': filtro
    })


def user_models(request, user):
    visited_user = get_object_or_404(CustomUser, pk=user)
    models = get_user_models(user)
    page = request.GET.get('page', 1)

    paginator = Paginator(models, 12)
    try:
        models = paginator.page(page)
    except PageNotAnInteger:
        models = paginator.page(1)
    except EmptyPage:
        models = paginator.page(paginator.num_pages)

    return render(request, 'visit_user_models.html', {'models': models, 'visited_user': visited_user.username})


def detail(request, id):
    '''
    View details of a scan model.
    :param request:
    :param id: Scan model ID
    :return:
    '''
    model = get_scan_model(id)
    if not model:
        print("Not model")
        raise Http404("scan does not exist")
    return render(request, 'detail.html', {
            'model': model,
            'model_url': 'http://2.152.105.193:8000/media/' + str(model.file)
        })


def guide(request):
    '''
    Display information about the web.
    :param request:
    :return:
    '''
    return render(request, 'guide.html')


def download_scannect(request):
    '''
    Display information about the web.
    :param request:
    :return:
    '''
    return render(request, 'download_scannect.html')


def stream(request):
    return render(request, 'viewer.html')
