from django.contrib import admin
from .models.scan_models import ScanModel
from .models.user_models import Profile
# Register your models here.
admin.site.register(ScanModel)
admin.site.register(Profile)