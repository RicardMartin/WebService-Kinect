from django.urls import path

from .views import user, visitor

urlpatterns = [

    # For unregistered users
    path('', visitor.home, name='home'),
    path('signup', visitor.signup, name='signup'),
    path('logout', visitor.logout_user, name='logout'),
    path('detail/<int:id>', visitor.detail, name='detail'),
    path('models/<int:user>', visitor.user_models, name='visit_user'),
    path('guide', visitor.guide, name='guide'),
    path('download_scannect', visitor.download_scannect, name='download_scannect'),
    path('stream', visitor.stream, name='stream'),

    # For logged in users
    path('upload', user.upload, name='upload'),
    path('upload/tmp', user.tmp_file, name='tmp_model'),
    path('myscans', user.user_models, name='myscans'),
    path('favourites', user.favourites, name='favourites'),
    path('profile', user.profile, name='profile'),
    path('edit/profile', user.settings, name='settings'),
    path('edit/password', user.change_password, name='change_password'),
    path('delete/user', user.delete_account, name='delete_account'),
    #path('edit/model/<int:id>', user.edit, name='edit'),
    path('delete/model/<int:id>', user.delete_model, name='delete_model'),
    path('upvote/<int:id>', user.upvote, name='upvote'),
    path('favourite/<int:id>', user.favourite, name='favourite'),

]
