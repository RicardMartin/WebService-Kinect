from django.apps import AppConfig


class ScannectWebConfig(AppConfig):
    name = 'scannect_web'
