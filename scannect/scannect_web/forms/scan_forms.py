from django import forms

from ..models.scan_models import ScanModel


class UploadFileForm(forms.ModelForm):
    class Meta:
        model = ScanModel
        fields = (
            'name',
            'file',
            'preview_image',
        )
