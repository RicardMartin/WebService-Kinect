from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from ..models.user_models import CustomUser, Profile


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(label="First name", max_length=25)
    last_name = forms.CharField(label="Last name", max_length=50)
    email = forms.EmailField(label="Email", max_length=254)

    class Meta:
        model = CustomUser
        fields = ('username',
                  'first_name',
                  'last_name',
                  'email',
                  'password1',
                  'password2',
                  )

    def save(self, commit=True):
        user = super(SignUpForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']

        if commit:
            user.save()

        return user


class LoginForm(forms.Form):
    class Meta:
        model = CustomUser
        fields = ('username',
                  'password',
                  )

    username = forms.CharField(max_length=25)
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if not user or not user.is_active:
            raise forms.ValidationError("Invalid login. Please try again.")
        return self.cleaned_data

    def login(self, request):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        return user


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('image',)


class EditUserForm(UserChangeForm):
    first_name = forms.CharField(label="First name", max_length=25)
    last_name = forms.CharField(label="Last name", max_length=50)
    email = forms.EmailField(label="Email", max_length=254)

    class Meta:
        model = CustomUser
        fields = ('username',
                  'first_name',
                  'last_name',
                  'email',
                  'password'
                  )

    def save(self, commit=True):
        user = super(EditUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']

        if commit:
            user.save()

        return user
