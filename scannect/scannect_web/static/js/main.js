$( document ).ready(function(){

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
   }

   var csrftoken = getCookie('csrftoken');

   function csrfSafeMethod(method) {
       // these HTTP methods do not require CSRF protection
       return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
   }

   $.ajaxSetup({
       beforeSend: function(xhr, settings) {
           if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
               xhr.setRequestHeader("X-CSRFToken", csrftoken);
           }
       }
   });

   $('button.upvote').on('click', function() {
        var scan_id = $(this).data('id');

        $.ajax({
            url: '/upvote/'+scan_id,
            method: 'POST',
            type: 'POST',
            success: function (data) {
                location.reload();
            },
            error: function (e) {
                alert('Something went wrong x_x');
            }
        });

   });

   $('button.favourite').on('click', function() {
        var scan_id = $(this).data('id');

        $.ajax({
            url: '/favourite/'+scan_id,
            method: 'POST',
            type: 'POST',
            success: function (data) {
                location.reload();
            },
            error: function (e) {
                console.log(e)
                alert('Something went wrong x_x');
            }
        });

    });

});