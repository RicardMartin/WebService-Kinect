$( document ).ready( function () {
    
    $('#color').on('change', function () {
        let color = $( this ).val();
        document.getElementById('vs_iframe').contentWindow.postMessage({msg_type:'set_color', value:color}, '*');
    });

    $('#bgcolor').on('change', function () {
        let color = $( this ).val();
        document.getElementById('vs_iframe').contentWindow.postMessage({msg_type:'set_bgcolor', value:color}, '*');
    });

    $('#shading').on('change', function () {
        let shading = $( this ).val();
        if (['flat', 'smooth', 'wireframe'].includes(shading)) {
            document.getElementById('vs_iframe').contentWindow.postMessage({msg_type:'set_shading', type:shading}, '*');
        }
    });

    $('#orientation').on('click', function () {
        let ori = $( this ).val();
        if (['front', 'back', 'left', 'right', 'top', 'bottom'].includes(ori)) {
            document.getElementById('vs_iframe').contentWindow.postMessage({msg_type:'set_orientation', value: ori}, '*');
        }
    });

    $('#take_photo').on('click', function () {
        document.getElementById('vs_iframe').contentWindow.postMessage({msg_type: 'get_photo'}, '*');
    });

    window.onmessage = function (e) {
        if ((e.origin == "http://www.viewstl.com") && (e.data.msg_type)) {
            if (e.data.msg_type == 'photo') {
                var model_img = document.createElement("img");
                model_img.src = e.data.img_data;
                model_img.height = '150';
                model_img.width = '230';
                $('#id_preview_image').val(model_img.src);
                $("#picture").html(model_img);
                $("#pictureTitle").html('Thumbnail image:');
            }
        }
    }
});