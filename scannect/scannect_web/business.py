import os

from django.db.models import Count
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import FileSystemStorage

from scannect.settings import BASE_DIR
from .models.scan_models import ScanModel
from threading import Timer
# from apscheduler.schedulers.background import BackgroundScheduler

def get_all_models(filter=None):
    '''
    Get scan models.
    :param filter: String to order models
    :return:
    '''
    if filter == "upvote":
        return ScanModel.objects.annotate(upvotes=Count('upvoted_scans')).order_by('-upvotes')
    if filter == "date_asc":
        return ScanModel.objects.order_by('creation_date')
    if filter == "date_desc":
        return ScanModel.objects.order_by('-creation_date')
    return ScanModel.objects.all()


def get_user_models(user):
    '''
    Get the user scan models.
    :param user: User instance
    :return:
    '''
    try:
        models = ScanModel.objects.filter(user=user)
    except Exception:
        return None
    return models


def get_user_favourites(user):
    '''
    Get the user favourite scan models.
    :param user: User instance
    :return:
    '''
    return user.profile.favourites.all()

def get_user_upvotes(user):
    return user.profile.upvotes.all()


def get_scan_model(id):
    '''
    Get a scan model.
    :param id: Scan model ID
    :return:
    '''
    try:
        return ScanModel.objects.get(pk=id)
    except ObjectDoesNotExist:
        return None


def save_preview_file(file):
    '''
    Save a temporary 3D file.
    :param file:
    :return:
    '''
    fs = FileSystemStorage(location=os.path.join(BASE_DIR, 'media/tmp'))
    filename = fs.save(file.name, file)
    t = Timer(10, removeTMP, [filename])
    t.start() 
    return 'http://2.152.105.193:8000/media/tmp/' + str(filename)


def removeTMP(filename):
    '''
    Delete a temporary file.
    :param filename:
    :return:
    '''
    os.remove(os.path.join(BASE_DIR, 'media/tmp/') + filename)
