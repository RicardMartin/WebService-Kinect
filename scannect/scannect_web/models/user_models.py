import os

from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.validators import ASCIIUsernameValidator
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from django.dispatch import receiver

from scannect.settings import MEDIA_ROOT
from .scan_models import ScanModel

def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.id, filename)


class CustomUser(User):
    class Meta:
        proxy = True

    username_validator = ASCIIUsernameValidator()


class Profile(models.Model):
    class Meta:
        db_table = 'profile'
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    upvotes = models.ManyToManyField(ScanModel, related_name='upvoted_scans')
    favourites = models.ManyToManyField(ScanModel, related_name='favourite_scans')
    image = models.ImageField(
                                upload_to=user_directory_path,
                                blank=True,
                                null=True
                             )

    def __str__(self):
        return self.user.username + ' Profile'


@receiver(post_save, sender=CustomUser)
def create_user_profile(sender, instance, created, **kwargs):
    '''
    Create user profile and images directory on user creation.
    :param sender: CustomUser object
    :param instance: CustomUser instance
    :param created: True if the user was created
    :param kwargs:
    :return:
    '''
    # Create user profile on user creation.
    if created:
        Profile.objects.create(user=instance)
        if not os.path.exists(os.path.join(MEDIA_ROOT, 'user_{0}/images/'.format(instance.id))):
            os.makedirs(os.path.join(MEDIA_ROOT, 'user_{0}/images').format(instance.id))
    instance.profile.save()


@receiver(post_save, sender=CustomUser)
def save_user_profile(sender, instance, **kwargs):
    '''
    Save user profile when user is modified.
    :param sender: CustomUser object
    :param instance: CustomUser instance
    :param kwargs:
    :return:
    '''
    instance.profile.save()


@receiver(pre_delete, sender=CustomUser)
def delete_user_profile(sender, instance, **kwargs):
    '''
    Delete user profile when user is deleted.
    :param sender: CustomUser object
    :param instance: CustomUser instance
    :param kwargs:
    :return:
    '''
    instance.profile.delete()


@receiver(post_delete, sender=CustomUser)
def auto_delete_picture_on_delete(sender, instance, **kwargs):
    '''
    Delete profile picture when a user is deleted.
    :param sender: CustomUser object
    :param instance: CustomUser instance
    :param kwargs:
    :return:
    '''
    if instance.profile.image and os.path.isfile(instance.profile.image.path):
        os.remove(instance.profile.image.path)


@receiver(pre_save, sender=Profile)
def auto_delete_file_on_change(sender, instance, **kwargs):
    '''
    Delete profile old picture when it's changed.
    :param sender: Profile object
    :param instance: Profile instance
    :param kwargs:
    :return:
    '''
    if not instance.pk:
        return False

    try:
        old_file = Profile.objects.get(pk=instance.pk).image
    except Profile.DoesNotExist:
        return False

    new_file = instance.image
    if old_file and not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)