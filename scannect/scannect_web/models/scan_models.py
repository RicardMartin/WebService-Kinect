import os

from django.db import models
from django.dispatch import receiver
from django.core.validators import FileExtensionValidator


def user_directory_path(instance, filename):
    '''
    Path to save scan models of an user.
    :param instance: Scan model instance.
    :param filename:
    :return: path
    '''
    return 'user_{0}/models/{1}'.format(instance.user.id, filename)


def user_image_path(instance, filename):
    '''
    Path to save the preview images of the scan models.
    :param instance: Scan model instance.
    :param filename:
    :return: path
    '''
    return 'user_{0}/images/{1}'.format(instance.user.id, filename)


class ScanModel(models.Model):
    class Meta:
        db_table = 'scan'

    user = models.ForeignKey(
                                'scannect_web.CustomUser',
                                on_delete=models.CASCADE
                            )
    name = models.CharField(max_length=50)
    creation_date = models.DateTimeField(auto_now_add=True)
    file = models.FileField(
                            upload_to=user_directory_path,
                            validators=[FileExtensionValidator(allowed_extensions=['stl', 'obj'])]
                            )
    preview_image = models.ImageField(
                                        upload_to=user_image_path,
                                        blank=True,
                                        null=True
                                     )

    def __str__(self):
        return self.user.username + ' - ' + self.name


@receiver(models.signals.post_delete, sender=ScanModel)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    '''
    Automatic delete of the 3D file when a scan model is deleted.
    :param sender: ScanModel object
    :param instance: ScanModel instance
    :param kwargs:
    :return:
    '''
    if instance.file and os.path.isfile(instance.file.path):
        os.remove(instance.file.path)


@receiver(models.signals.pre_save, sender=ScanModel)
def auto_delete_file_on_change(sender, instance, **kwargs):
    '''
    Automatic delete of the old 3D file when is changed.
    :param sender: ScanModel object
    :param instance: ScanModel instance
    :param kwargs:
    :return:
    '''
    if not instance.pk:
        return False

    try:
        old_file = ScanModel.objects.get(pk=instance.pk).file
    except ScanModel.DoesNotExist:
        return False

    new_file = instance.file
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)