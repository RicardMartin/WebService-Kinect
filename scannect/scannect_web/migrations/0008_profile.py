# Generated by Django 2.0.4 on 2018-05-03 17:34

from django.db import migrations, models
import django.db.models.deletion
import scannect_web.models.user_models


class Migration(migrations.Migration):

    dependencies = [
        ('scannect_web', '0007_auto_20180419_1542'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(default='/static/img/shrek.jpg', upload_to=scannect_web.models.user_models.user_directory_path)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='scannect_web.CustomUser')),
            ],
            options={
                'db_table': 'profile',
            },
        ),
    ]
