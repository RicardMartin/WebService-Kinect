# Generated by Django 2.0.4 on 2018-05-03 17:45

from django.db import migrations, models
import scannect_web.models.user_models


class Migration(migrations.Migration):

    dependencies = [
        ('scannect_web', '0008_profile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='image',
            field=models.ImageField(default='static/img/shrek.jpg', upload_to=scannect_web.models.user_models.user_directory_path),
        ),
    ]
