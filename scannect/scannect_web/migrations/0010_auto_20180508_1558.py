# Generated by Django 2.0.4 on 2018-05-08 15:58

import django.core.validators
from django.db import migrations, models
import scannect_web.models.scan_models
import scannect_web.models.user_models


class Migration(migrations.Migration):

    dependencies = [
        ('scannect_web', '0009_auto_20180503_1745'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=scannect_web.models.user_models.user_directory_path),
        ),
        migrations.AlterField(
            model_name='scanmodel',
            name='file',
            field=models.FileField(upload_to=scannect_web.models.scan_models.user_directory_path, validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['stl', 'obj'])]),
        ),
    ]
