
// Para que funcione en canvas necesitamos tres objetos principales
// La escena, la camara y el render
var renderer = undefined
var scene = undefined
var camera = undefined

function loadObject(stlFile, loadBtn) {

    // To color the object
    var ambientLightColor = 0xcccccc;
    var DirectionalLightColor = 0xffffff;
    var objectMeshColor = 0x4c4c4c;
    // To color the background
    var fogColor = 0x329932;

    // height i width del canvas
    var w = 600;
    var h = 400;

    // Create the container div so we can set width and height
    var containerDiv = document.createElement('div');
    containerDiv.style.width = w + "px";
    containerDiv.style.height = h + "px";
    containerDiv.id = "previewContainer";

    // Escena
    scene = new THREE.Scene();

    // Camara
    camera = new THREE.PerspectiveCamera( 75, w/h, 0.1, 2000 );

    // Render
    renderer = new THREE.WebGLRenderer({
        preserveDrawingBuffer   : true
    });
    renderer.setSize( w, h);
    renderer.domElement.id = 'previewCanvas';

    //var view = document.getElementById("view");
    containerDiv.appendChild( renderer.domElement );

    // Objeto cubo
    var geometry = new THREE.BoxGeometry( 1, 1, 1 );
    var material = new THREE.MeshPhongMaterial( {color:objectMeshColor, shading: THREE.SmoothShading} );
    var object = new THREE.Mesh( geometry, material );
    scene.add( object );

    // Light
    scene.add(new THREE.AmbientLight(ambientLightColor));
    var light1 = new THREE.DirectionalLight(DirectionalLightColor);
    light1.position.set(0, 100, 100);
    scene.add(light1);
    var light2 = new THREE.DirectionalLight(DirectionalLightColor);
    light2.position.set(0, -100, -100);
    scene.add(light2);

    // Background
    var backgroundMaterial = new THREE.MeshPhongMaterial({color:0xCFE2F3,emissive: 0xCFE2F3, shading: THREE.SmoothShading, fog: true, side: THREE.BackSide});
    var background = new THREE.Mesh( new THREE.CubeGeometry( 1000, 1000, 1000 ), backgroundMaterial );
    scene.add( background );

    // Fog
    scene.fog = new THREE.FogExp2(fogColor, 0.0048);

    camera.position.z = 5;
    document.body.appendChild( containerDiv );

    // Scene controls
    var controls = new THREE.TrackballControls(camera, containerDiv);
    controls.maxDistance = 100;
    controls.minDistance = 1;

    // Local Function to update canvas
    function animateCanvas() {
        requestAnimationFrame( animateCanvas );
        controls.update();
        renderer.render( scene, camera );
    }
    animateCanvas();

    // Capture button
    if (loadBtn) {
        var capture = document.createElement( "div" );
        capture.innerHTML = '<button type="button" class="btn" title="Take picture">Capturasion</button>';
        //capture.setAttribute( "class", "thing-page-image-opts");
        capture.style.position = "relative";
        capture.style.top = "10px";
        capture.style.right = "10px";
        capture.addEventListener('click', function(){
            var dataImage = $('#previewCanvas')[0].toDataURL();
            var previewImage = document.createElement( "img" );
            previewImage.src = dataImage;
            document.body.appendChild( previewImage );
        });
        $('#previewContainer').append( capture );
    }

    // Open STL Files
    var openFile = function (stlFile) {
        var reader = new FileReader();
        reader.addEventListener("load", function (ev) {
            var buffer = ev.target.result;
            var geom = loadStl(buffer);
            scene.remove(object);
            object = new THREE.Mesh(geom, material);
            object.geometry.computeBoundingBox();
            camera.position.x = object.geometry.boundingBox.min.x * 0.7;
            camera.position.y = object.geometry.boundingBox.max.y * 1.5;
            camera.position.z = object.geometry.boundingBox.max.z * 1.8;
            controls.maxDistance = object.geometry.boundingBox.max.y * 7;
            controls.minDistance = object.geometry.boundingBox.min.x;
            scene.add(object);
        }, false);
        reader.readAsArrayBuffer(stlFile);
    };
    openFile(stlFile);



}
