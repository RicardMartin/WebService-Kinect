/**
 * Funcion que crea el visor STL en el container indicado
 * @param container
 * @param width
 * @param height
 * @returns {{readStl: readStl, readObj: readObj, getRender: function(): *, toggleControls: toggleControls}}
 */
function createViewer(container, width, height) {
    container = container || document.body
    var camera, scene, renderer, geometry, material, mesh, keyLight, spotLight, vs, controls
    var orgX, orgY
    load()
    return {
        readStl: function (stl) {
            buildGeometry(stl, {name: 'file.stl'})
        },
        readObj: function (obj) {
            buildGeometry(obj, {name: 'file.obj'})
        },
        getRender: function () {
            return renderer
        },
        toggleControls: function (status) {
            controls.enabled = status
        }
    }

    /**
     * Inicializamos todas las variables necesarias para el funcionamiento de visor
     * - Camara
     * - Escena
     * - Materiales
     * - Iluminación
     * - Controles
     */
    function init() {
        var backgroundcolor = 0xfbfafb
        // CAMERA
        camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 10000)
        camera.position.z = 500
        camera.up = new THREE.Vector3(0, 1, 0)
        camera.lookAt(new THREE.Vector3(0, 0, 0))
        // SCENE & FOG
        scene = new THREE.Scene()
        scene.fog = new THREE.Fog(backgroundcolor, 10, 5000)
        scene.add(camera)
        // MATERIAL
        materials = [new THREE.MeshPhongMaterial({
            color: 0xA6ce39,
            ambient: 0x154800,
            specular: 0x444444,
            shininess: 7,
            refractionRatio: 1.46,
            wrapAround: true
        })]
        // LIGHTS & SHADOWS
        keyLight = new THREE.DirectionalLight(0xffffff);
        keyLight.intensity = .9;
        keyLight.position.set(500, 140, 500);
        keyLight.castShadow = true;
        keyLight.shadowMapHeight = 2048
        keyLight.shadowMapWidth = 2048
        keyLight.shadowDarkness = .15
        spotLight = new THREE.PointLight(0xffffff);
        spotLight.intensity = .5
        spotLight.position.set(-500, 140, -500);
        camera.add(spotLight)
        camera.add(keyLight);
        // RENDER
        if ("WebGLRenderingContext" in window) renderer = new THREE.WebGLRenderer({
            antialias: true,
            preserveDrawingBuffer: true
        })
        else renderer = new THREE.CanvasRenderer()
        renderer.setSize(width, height)
        renderer.shadowMapEnabled = true
        renderer.shadowMapType = THREE.PCFShadowMap
        renderer.shadowMapSoft = true
        renderer.setClearColor(backgroundcolor, 0);
        // CONTROLS
        /*
        controls = new THREE.OrbitControls(camera, renderer.domElement)
        controls.rotateSpeed = 2.0
        controls.zoomSpeed = 1.2
        controls.panSpeed = 0.8
        controls.noZoom = false
        controls.noPan = false
        controls.staticMoving = true
        controls.dynamicDampingFactor = 0.1
        controls.keys = [65, 83, 68]
        controls.addEventListener('change', render)
        */
        controls = new THREE.TrackballControls(camera, renderer.domElement);
        controls.maxDistance = 100;
        controls.minDistance = 1;
        container.appendChild(renderer.domElement)
    }

    /**
     * Bucle de animación
     */
    function animate() {
    setTimeout( function() {
        requestAnimationFrame( animate );
    }, 1000  );
    render()
    controls.update()
  }

    /**
     * Renderiza la escena
     */
  function render() {
    renderer.render( scene, camera )
  }
   /**
    * Función principal
    */
  function load() {
    init()
     animate()
  }

  /** CARGA DE ARCHIVOS **/

  function readStl(oFile, vs, fs) {
    if (oFile instanceof ArrayBuffer) return arrayBufferToBinaryString(oFile, function(stl) {
      readBinaryStl(stl, vs, fs)
    })
    var solididx = oFile.search("solid")
    if (solididx > -1 && solididx < 10) {
      var l = oFile.split(/[\r\n]/g)
      readAsciiStl(l, vs, fs)
      if(fs.length == 0) readBinaryStl(oFile, vs, fs)
    } else {
      readBinaryStl(oFile, vs, fs)
    }
  }

  // Read ascii stl files
  function readAsciiStl(l, vs, fs) {
    var solid = false
    var face = false
    var vis = []
    vtest = {}
    for(var i=0; i < l.length; i++) {
      var line = l[i]
      if (solid) {
        if (line.search("endsolid") > -1) solid = false
        else if(face) {
          if (line.search("endfacet") > -1) {
            face = false
            var f = new THREE.Face3(vis[0], vis[1], vis[2])
            fs.push(f)
          } else if (line.search("vertex") > -1) {
            var cs = line.substr(line.search("vertex") + 7)
            cs = cs.trim()
            var ls = cs.split(/\s+/)
            var v = new THREE.Vector3(parseFloat(ls[0]), parseFloat(ls[1]), parseFloat(ls[2]))
            var vi = vs.length
            if (cs in vtest) {
              vi = vtest[cs]
            } else {
              vs.push(v)
              vtest[cs] = vi
            }
            vis.push(vi)
          }
        }
        else {
          if (line.search("facet normal") > -1) {
            face = true
            vis = []
          }
        }
      }
      else {
        if (line.search("solid")> - 1) solid = true
      }
    }
    vtest = null
  }
  function triangle() {
    if (arguments.length == 2) {
      this._buffer = arguments[0]
      this._sa = arguments[1]
    } else {
      this._sa = 0
      this._buffer = new ArrayBuffer(50)
    }
    this.__byte = new Uint8Array(this._buffer)
    this.normal = new Float32Array(this._buffer, this._sa + 0, 3)
    this.v1 = new Float32Array(this._buffer, this._sa + 12, 3)
    this.v2 = new Float32Array(this._buffer, this._sa + 24, 3)
    this.v3 = new Float32Array(this._buffer, this._sa + 36, 3)
    var _attr = new Int16Array(this._buffer, this._sa + 48, 1)
    Object.defineProperty(this, "attr", {
      get: function(){
        return _attr[0]
      },
      set: function(val) {
        _attr[0] = val
      },
      enumerable: true
    })
  }
  // Read binary stl files
  function readBinaryStl(l, vs, fs) {
    var buf = new ArrayBuffer(l.length)
    var bbuf = new Uint8Array(buf)
    for (var i = 0; i < l.length; i++) bbuf[i] = l.charCodeAt(i)
    var trnr = new Uint32Array(buf, 80, 1)
    var vis = [0, 0, 0]
    var vtest = {}
    var offset = 84
    var face = new triangle()
    for (var i = 0; i < trnr[0]; i++) {
      for (var j = 0; j < 50; j++) face.__byte[j] = bbuf[offset + j]
      var v = new THREE.Vector3(face.v1[0], face.v1[1], face.v1[2])
      var k = "" + face.v1[0] + "," + face.v1[1] + "," + face.v1[2]
      vis[0] = vs.length
      if (k in vtest) vis[0] = vtest[k]
      else {
        vs.push(v)
        vtest[k] = vis[0]
      }
      v = new THREE.Vector3(face.v2[0], face.v2[1], face.v2[2])
      k = "" + face.v2[0] + "," + face.v2[1] + "," + face.v2[2]
      vis[1] = vs.length
      if (k in vtest) vis[1] = vtest[k]
      else {
        vs.push(v)
        vtest[k] = vis[1]
      }
      v = new THREE.Vector3(face.v3[0], face.v3[1], face.v3[2])
      k = "" + face.v3[0] + "," + face.v3[1] + "," + face.v3[2]
      vis[2] = vs.length
      if (k in vtest) vis[2] = vtest[k]
      else {
        vs.push(v)
        vtest[k] = vis[2]
      }
      var normal = new THREE.Vector3( face.normal[0], face.normal[1], face.normal[2] )
      var f = new THREE.Face3(vis[0], vis[1], vis[2], normal)
      fs.push(f)
      offset += 50
    }
    vtest = null
    delete bbuf
    delete buf
    buf = null
  }

}

