$( document ).ready( function() {
    
    $("#id_file").on('change', function(ev) {

        var stl_file = ev.target.files[0];

        var data = new FormData();
        data.append('stl_file', stl_file);

        $.ajax({
            url: '/upload/tmp',
            method: 'POST',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (data) {
                $('#preview').html('<iframe id="vs_iframe" src="http://www.viewstl.com/?embedded&url=' + data.url + '&bgcolor=green&color=white&shading=flat&rotation=yes&clean=yes&noborder=yes&orientation=front&edges=no" style="border:0;margin:0;width:100%;height:100%;"></iframe>');
            },
            error: function (e) {
                alert('Something went wrong x_x');
            }
        });

    })

});
