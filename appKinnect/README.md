# Readme de [libfreenect](https://github.com/OpenKinect/libfreenect)

Instalamos libfreenect:

    # dnf install libfreenect

Para comprobar que está cogiendo bien el Kinect (saldrá una pantalla retransmitiendo desde kinect):

    $ freenect-glview

Instalar libusb, gcc-c++, libusbx-devel, cmake, python-numpy redhat-rpm-config y python3 (con un dnf install ya vale, puede dar problemas la versión de gcc, con la opción --allowerasing te aseguras de que sean compatibles).

	# dnf install -y libusb gcc-c++ gcc libusbx-devel cmake python-numpy redhat-rpm-config python python-devel --allowerasing

Clonamos libfreenect de github, tenemos que hacer su build y el del lenguaje que vayamos a utilizar.
El build de libfreenect:

	$ git clone git://github.com/OpenKinect/libfreenect.git
    $ cd libfreenect
    $ mkdir build
    $ cd build
    $ cmake -L ..
    $ make

Esto es cosa del Sr Patel:

	$ sudo make install

Para hacer el build de python wrapper:

    $ cmake .. -DCMAKE_BUILD_PYTHON=ON
    $ make


# Wordpress del [Señor Patel](https://naman5.wordpress.com/2014/06/24/experimenting-with-kinect-using-opencv-python-and-open-kinect-libfreenect/)

Para saber si opengl esta presente :)

	# glxinfo

Poder conectarse a Kinect sin ser sudo:

	# usermod -G video $USER

Utilizar librerías compartidas:

	$ sudo ldconfig /usr/local/lib64/

En /etc/udev/rules.d/51-kinect.rules:

	# ATTR{product}=="Xbox NUI Motor"
	SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02b0", MODE="0666"
	# ATTR{product}=="Xbox NUI Audio"
	SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02ad", MODE="0666"
	# ATTR{product}=="Xbox NUI Camera"
	SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02ae", MODE="0666"
	# ATTR{product}=="Xbox NUI Motor"
	SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02c2", MODE="0666"
	# ATTR{product}=="Xbox NUI Motor"
	SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02be", MODE="0666"
	# ATTR{product}=="Xbox NUI Motor"
	SUBSYSTEM=="usb", ATTR{idVendor}=="045e", ATTR{idProduct}=="02bf", MODE="0666"



Dependencias pip -> numpy, opencv-python (opencv instala numpy):

	$ sudo pip install opencv-python

En /etc/environment añadir QT_X11_NO_MITSHM=1

En libfreenect/wrappers/python:

	$ sudo python setup.py install
	$ python demo_cv2_sync.py
	
Recomendado:
    
    # dnf reinstall python3-pip
    
Dentro del virtual environment:
    
    # pip3 install --upgrade pip
    # pip3 install django

Y ya estaría.

A tener en cuenta

    pip install pyopenssl
