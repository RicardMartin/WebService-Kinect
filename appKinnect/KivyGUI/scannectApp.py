import sys
import cv2
import freenect
import frame_convert2
import webbrowser
from PyQt5.QtCore import pyqtSlot, Qt, pyqtSignal
from PyQt5.QtWidgets import QApplication, QDialog, QLabel, QFileDialog
from PyQt5.uic import loadUi
from PyQt5.QtGui import QPixmap, QImage  

class ScannectGUI(QDialog):
    
	currentDegs = 0
	ctx = freenect.init()
	mdev = None
	videoRunning = False
	mdev = None
	taken_photos = 0
	depth = []
	video = []
	savesPath = '/tmp'

	def __init__(self):
		super(ScannectGUI,self).__init__()
		loadUi('scannectUI.ui',self)
		self.setWindowTitle('Scannect')
		self.show()
		self.frame = None
		self.record.clicked.connect(self.capturePointCloud)
		self.startButton.clicked.connect(self.loadDepth)
		self.stopButton.clicked.connect(self.stopKinnect)
		self.lowerTreshold.valueChanged.connect(self.updateLowerThreshValue)
		self.higherTreshold.valueChanged.connect(self.updateHigherThreshValue)
		self.urlButton.clicked.connect(self.openUrl)
		self.dirChooser.clicked.connect(self.chooseDir)
		self.workingDir.setText("Your saves will be stored in:\n\n" + str(self.savesPath))
		self.setTiltKinnect()
		#loadUi('scannectUI.ui',self)
		#self.exceptionLabel.setText(e)
		#self.show()



	def capturePointCloud(self):
		# Create meshlab file
		meshlab_file = open(str(self.savesPath) + '/myScan-' + str(self.taken_photos) + '.ply', 'wt')
		print(str(meshlab_file))
		# 640 x 480 save image state
		index = 0
		# Iterate through image information and save if necessary
		lines = ''

		for y in range(0, 480):
		    for x in range(0, 640):
		        # Points within the treshold values
		        if self.depth[y][x] >= self.lowerTreshold.value() and self.depth[y][x] <= self.higherTreshold.value():
		            #lines += str(x) + " " + str(y) + " " + str(depth[y][x]) + " " + str(video[y][x][2]) + " " + str(video[y][x][1]) + " " + str(video[y][x][0]) + "\n"
		            lines += str(x) + " " + str(y) + " " + str(self.depth[y][x]) + "\n"
		            index += 1
		    # state information (progressbar)
		    self.captureProgress.setValue(int(float(y) / float(480) * 100))

		self.captureProgress.setValue(100)
		self.taken_photos += 1

		meshlab_file.write("ply\nformat ascii 1.0\nelement vertex " + str(index) + "\n")
		meshlab_file.write("property float x\n")
		meshlab_file.write("property float y\n")
		meshlab_file.write("property float z\n")

		# Guardar color 
		#meshlab_file.write("property uchar red\n")
		#meshlab_file.write("property uchar green\n")
		#meshlab_file.write("property uchar blue\n")

		meshlab_file.write("end_header\n")

		meshlab_file.write(lines)
		meshlab_file.close()

	@pyqtSlot()
	def loadDepth(self):
	    self.videoRunning = True
	    self.startButton.setEnabled(not self.videoRunning)
	    self.stopButton.setEnabled(self.videoRunning)
	    self.record.setEnabled(True)

	    if not self.mdev:
	        freenect.close_device(self.mdev)

	    while self.videoRunning:
	        self.display_depth()
	        QApplication.processEvents()

	    self.frame = None
	    self.depth = None
	    QApplication.processEvents()
        
	def chooseDir(self):
		self.savesPath = QFileDialog.getExistingDirectory(self,"Choose a directory", "/tmp")
		self.workingDir.setText("Your saves will be stored in:\n\n  " + str(self.savesPath))

	def updateLowerThreshValue(self):
	    self.lowerTresholdLabel.setText(str(self.lowerTreshold.value()) + " mm")

	def updateHigherThreshValue(self):
	    self.higherTresholdLabel.setText(str(self.higherTreshold.value()) + " mm")

	def setTiltKinnect(self):
	    self.mdev = freenect.open_device(self.ctx,0)
	    freenect.set_tilt_degs(self.mdev, 0)
	    freenect.close_device(self.mdev)

	def stopKinnect(self):
	    self.videoRunning = False
	    self.startButton.setEnabled(not self.videoRunning)
	    self.stopButton.setEnabled(self.videoRunning)
	    self.record.setEnabled(False)
	    QApplication.processEvents()

	def closeEvent(self,event):
	    self.videoRunning = False
	    freenect.shutdown(self.ctx)

	def openUrl(self):
		webbrowser.open("http://2.152.105.193:8000/guide")

	def display_depth(self):
	    self.depth = freenect.sync_get_depth(0, freenect.DEPTH_REGISTERED)[0]
	    self.frame = 255 * freenect.np.logical_and(self.depth >= self.lowerTreshold.value(), self.depth <= self.higherTreshold.value())
	    self.frame = self.frame.astype(freenect.np.uint8)
	    qformat = QImage.Format_Indexed8
	    img = QImage(self.frame, self.frame.shape[1], self.frame.shape[0], self.frame.strides[0],qformat)
	    self.FrameLabel.setPixmap(QPixmap.fromImage(img))
	    self.FrameLabel.setAlignment(Qt.AlignHCenter|Qt.AlignVCenter)
	    QApplication.processEvents()

app=QApplication(sys.argv)
widget=ScannectGUI()
sys.exit(app.exec_())

