import freenect
import cv2
import frame_convert2
import signal
import numpy
import sys

# Init context
ctx = freenect.init()
mdev = freenect.open_device(ctx,0)
freenect.set_depth_mode(mdev, freenect.RESOLUTION_MEDIUM, freenect.DEPTH_REGISTERED)

# Global variables
keep_running = True
taken_photos = 0
depth_data = []
back_clipping = 2500
inverted_depth = False
max_depth = 2500
min_depth = 500
trackbar_max_depth = 3000
trackbar_min_depth = 1000

# Inital calibrations
tilt_degs = 10
calibrate_kin_degs = True

# Callback - Change max depth value
def change_max_depth(md):
    global max_depth
    max_depth = md

# GUI Windows
cv2.namedWindow('RGB', cv2.WINDOW_GUI_NORMAL)
cv2.namedWindow('Depth', cv2.WINDOW_GUI_NORMAL)
cv2.resizeWindow('RGB',640,480)
cv2.resizeWindow('Depth',640,480)
cv2.createTrackbar('Maximum Depth', 'Depth', max_depth, trackbar_max_depth, change_max_depth)
cv2.setTrackbarMin('Maximum Depth', 'Depth', trackbar_min_depth)


########### HANDLERS ###########

# Captures Death signal Ctrl+C to stop the process
def handler(signum, frame):
    """Sets up the kill handler, catches SIGINT"""
    global keep_running
    keep_running = False

# Handles key event of display_depth
def handle_key_event(keypressed,data):
	# use globals in context
	global keep_running, inverted_depth, tilt_degs, calibrate_kin_degs, depth_data
	# keyrequest for ESC Key
	if keypressed == 27:
		keep_running = False
	# keyrequest for capuring
	if keypressed == 32:
		save_3d_information(depth_data, data)

	if keypressed == 119:
		if tilt_degs < 16:
			tilt_degs += 1
		calibrate_kin_degs = True

	if keypressed == 115:
		if tilt_degs > -16:
			tilt_degs -= 1
		calibrate_kin_degs = True
	if keypressed == 113:
		printInfo();

########### DISPLAYERS ###########

# Displays Depth information
def display_depth(dev, data, timestamp):
    global depth_data
    display_data = 255 * numpy.logical_and(data >= min_depth, data <= max_depth)
    display_data = display_data.astype(numpy.uint8)
    cv2.imshow('Depth', display_data)
    keypressed = cv2.waitKey(10)
    depth_data = data
    handle_key_event(keypressed,data)

# Displays RGB information
def display_rgb(dev, data, timestamp):
    cv2.imshow('RGB', frame_convert2.video_cv(data))
    keypressed = cv2.waitKey(10)
    handle_key_event(keypressed,data)


def printInfo():
	global mdev
	print('------ Device Info -------')
	#print('- Current Accel: ' + str(freenect.get_accel(mdev)))
	#print('- Current Tilt Degs: ' + str(freenect.get_tilt_degs(mdev)))
	#print('- Current Titl State: ' + str(freenect.get_tilt_state(mdev)))
	print('- Current Depth Format: ' + str(freenect.get_depth_format(mdev)))
	print('- Current Video Format: ' + str(freenect.get_video_format(mdev)))


# Save 3D information in ply File
def save_3d_information(depth, video):
    # Use globals in context
    global taken_photos, max_depth, min_depth
    # Create meshlab file
    meshlab_file = open('meshlab-' + str(taken_photos) + '.ply', 'wt')
    video_image = numpy.zeros((640, 480, 3), numpy.uint8)

    # Convert image
    video = video[:, :, ::-1]
    # 640 x 480 save image state
    index = 0
    print("start analysing ...")
    # Iterate through image information and save if necessary
    lines = ''
    for y in range(0, 480):
        for x in range(0, 640):
            # Points within 0,5m and 2,5m
            if depth[y][x] >= min_depth and depth[y][x] <= max_depth:
                lines += str(x) + " " + str(y) + " " + str(depth[y][x]) + " " + str(video[y][x][2]) + " " + str(
                    video[y][x][1]) + " " + str(video[y][x][0]) + "\n"
                # COLORES 0 = B 1 = G 2 = R
                cv2.line(video_image, (x, y), (x, y),
                         (float(video[y][x][0]), float(video[y][x][1]), float(video[y][x][2])))
                index += 1
        # state information (progressbar)
        percent = int(float(y) / float(480) * 100)
        sys.stdout.write(str(percent).zfill(2) + " % - ")
        # output
        for i in range(0, int(percent / 2)):
            sys.stdout.write("#")
        sys.stdout.write("\r")
        sys.stdout.flush()

    # PLY HEADER AND FILE CREATION
    meshlab_file.write("ply\nformat ascii 1.0\nelement vertex " + str(index) + "\n")
    meshlab_file.write("property float x\n")
    meshlab_file.write("property float y\n")
    meshlab_file.write("property float z\n")
    meshlab_file.write("property uchar red\n")
    meshlab_file.write("property uchar green\n")
    meshlab_file.write("property uchar blue\n")
    meshlab_file.write("end_header\n")

    meshlab_file.write(lines)

    print("\nsave 3d information ... wrote " + str(index) + " points")
    meshlab_file.close()
    cv2.imwrite("video-result-" + str(taken_photos) + ".png", video_image)
    taken_photos += 1


########### MAIN LOOP ###########

def body(*args):
    global calibrate_kin_degs, tilt_degs

    if calibrate_kin_degs:
        freenect.set_tilt_degs(args[0], tilt_degs)
        calibrate_kin_degs = False

    if not keep_running:
        raise freenect.Kill()
    signal.signal(signal.SIGINT, handler)


print('Press ESC in window to stop')
freenect.runloop(dev=mdev, depth=display_depth, video=display_rgb, body=body)
