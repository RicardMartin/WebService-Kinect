# Readme de [libfreenect](https://github.com/OpenKinect/libfreenect)

Instalamos libfreenect:

    # dnf install libfreenect

Para comprobar que está cogiendo bien el Kinect (saldrá una pantalla retransmitiendo desde kinect):

    $ freenect-glview

Instalar libusb, gcc-c++, libusbx-devel, cmake, python-numpy redhat-rpm-config y python3 (con un dnf install ya vale, puede dar problemas la versión de gcc, con la opción --allowerasing te aseguras de que sean compatibles).

	# dnf install -y libusb gcc-c++ gcc libusbx-devel cmake python-numpy redhat-rpm-config python python-devel python-magic --allowerasing

Clonamos libfreenect de github, tenemos que hacer su build y el del lenguaje que vayamos a utilizar.
El build de libfreenect:

	$ git clone git://github.com/OpenKinect/libfreenect.git
    $ cd libfreenect
    $ mkdir build
    $ cd build
    $ cmake -L ..
    $ make

Esto es cosa del Sr Patel:

	$ sudo make install
# INICIAR SERVIDOR MEDIANTE DOCKER

Primero instalamos docker y docker compose:

    # dnf -y install docker docker-compose

Iniciamos el servicio

    # systemctl start docker
    # systemctl enable docker

Creamos el grupo docker y añadimos el usuario pueda utilizar el servicio. Salimos de la sessión y volvemos a entrar (Si no funciona reiniciamos el sistema).

    # groupadd docker
    $ sudo usermod -a -G docker $USER

Para evitar problemas de permisos al ejecutar manage.py mediante el docker:

    $ setenforce 0

Ahora ejecutamos el servidor, en la carpeta en la que esté el docker-compose ejecutamos:

    $ docker-compose up

Tardará unos minutos, creará tres imágenes (python3, postgres y la imagen propia del servidor) y dos containers (uno ejecutará django y el otro tendrá el servidor)
